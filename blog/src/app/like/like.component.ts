import { Component, Input } from '@angular/core';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { ArtikelService } from '../artikel.service';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.scss'],
})
export class LikeComponent {

  @Input() artikel?: any;

  fathumbsup = faThumbsUp;
  numberOfLikes?:number
  constructor(public artikelService: ArtikelService) {}


  ngOnInit(): void {
    this.numberOfLikes = this.artikel.likes;
  }

  likeButtonClick() {
    if(this.numberOfLikes !== undefined){
      this.numberOfLikes++;
      this.artikel.likes++;
    }
  }
}

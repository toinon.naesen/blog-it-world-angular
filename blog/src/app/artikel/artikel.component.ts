import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArtikelService } from '../artikel.service';
import { faComment } from '@fortawesome/free-solid-svg-icons';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-artikel',
  templateUrl: './artikel.component.html',
  styleUrls: ['./artikel.component.scss']
})
export class ArtikelComponent implements OnInit {

  ingelogdeGebruiker?:string;
  tempObj?:any;
  id?:number;
  titel?:string;
  foto?:any;
  alineas?:any;
  tussentitels?:any;
  afbeeldingen?:any;
  comments?:any;
  likes?:number;
  datum?:string;
  auteur?: string;
  faComment = faComment;
  fathumbsUp = faThumbsUp;

  @Input() artikel?: any;

  artikels;
  constructor(private route: ActivatedRoute, public artikelService: ArtikelService ) {
    this.artikels = artikelService.artikels;
    this.ingelogdeGebruiker = artikelService.ingelogdeGebruiker;
  }

  PlaatsComment(){
    this.tempObj.comments.unshift( 
      { user: this.ingelogdeGebruiker, comment:  (<HTMLInputElement>document.getElementById("reactie")).value , avatar: 'assets/images/avatar8.jpg'});
  }

  VerwijderArtikel(){
    this.artikelService.VerwijderArtikel(this.id ?? 0);
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.id = params['id'];
      this.tempObj = this.artikels.find((x) => x.id == this.id);
      this.titel = this.tempObj?.titel;
      this.foto = this.tempObj?.foto;
      this.alineas = this.tempObj?.alineas;
      this.tussentitels = this.tempObj?.tussentitels;
      this.afbeeldingen = this.tempObj?.afbeeldingen;
      this.comments = this.tempObj?.comments;
      this.likes = this.tempObj?.likes;
      this.datum = this.tempObj?.datum;
      this.auteur = this.tempObj?.auteur;
    });
  }
}

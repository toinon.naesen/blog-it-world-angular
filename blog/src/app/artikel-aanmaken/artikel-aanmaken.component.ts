import { Component, OnInit, Input } from '@angular/core';
import { ArtikelService } from '../artikel.service';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-artikel-aanmaken',
  templateUrl: './artikel-aanmaken.component.html',
  styleUrls: ['./artikel-aanmaken.component.scss'],
})
export class ArtikelAanmakenComponent implements OnInit {
  fatCheck = faCheckCircle;
  tags?: any;
  aangeMaaktArtikelId?: number;
  constructor(public artikelService: ArtikelService) {
    this.tags = artikelService.tags;
    this.aangeMaaktArtikelId = artikelService.nieuwArtikelID
  }
  VoegArtikelToe() {

    let allTags =  (<HTMLInputElement>document.getElementById('tags')).value.split(",");
    let ingelogd = this.artikelService.ArtikelToevoegen(

      (<HTMLInputElement>document.getElementById('exampleFormControlInput1'))
        .value,
      (<HTMLInputElement>document.getElementById('exampleFormControlTextarea1'))
        .value,
        allTags
    );
    if(ingelogd){
      document.getElementById('overlayMessage')?.classList.remove('noDisplay');
      document.getElementById('overlayMessage')?.classList.add('overLayShow');
      this.aangeMaaktArtikelId = this.artikelService.nieuwArtikelID -1;
    }else{
      alert("U moet ingelogd zijn voor een artikel te kunnen toevoegen!");
    }
  }

  VoegTagToe(dezeTag: string) {
    let allTags =  (<HTMLInputElement>document.getElementById('tags')).value.split(",");
    if(allTags[0] == ""){
      allTags.length=0;
    }
    if(allTags.length == 0 || allTags.indexOf(dezeTag) == -1){
      allTags.push(dezeTag);
      (<HTMLInputElement>document.getElementById('tags')).value = allTags.join(",");
    }
    else{
      allTags.splice(allTags.indexOf(dezeTag),1);
      (<HTMLInputElement>document.getElementById('tags')).value = allTags.join(",");
    }
  }

  SluitVenster() {
    document.getElementById('overlayMessage')?.classList.remove('overLayShow');
    document.getElementById('overlayMessage')?.classList.add('noDisplay');
  }

  ngOnInit(): void {}
}

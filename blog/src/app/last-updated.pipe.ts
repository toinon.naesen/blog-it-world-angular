import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lastUpdated'
})
export class LastUpdatedPipe implements PipeTransform {

  transform(value:string): string {
    //Bereken tijdverschil, als het korter dan een dag is return vandaag.
    var time = Date.parse(value);
    var thisDate = Date.now();
    var Difference_In_Time = Math.floor((thisDate - time)/1000/60/60/24);
    if (Difference_In_Time<1) {
      return "Laatst gewijzigd vandaag.";
    }else{
      return "Laatst gewijzigd " + Difference_In_Time.toString()+ " dagen geleden.";
    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AccesGuard } from './acces.guard';
import { ArtikelAanmakenComponent } from './artikel-aanmaken/artikel-aanmaken.component';
import { ArtikelComponent } from './artikel/artikel.component';
import { Error404Component } from './error404/error404.component';
import { FilterComponent } from './filter/filter.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path:"", component: HomeComponent},
  {path:"about", component: AboutComponent},
  {path:"artikel", component: ArtikelComponent},
  {path:"filter", component: FilterComponent},
  {path:"artikelAanmaken", component: ArtikelAanmakenComponent, canActivate: [AccesGuard]},
  {path:"login", component: LoginComponent},
  {path:"**", component: Error404Component}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit,Input } from '@angular/core';
import { ArtikelService } from '../artikel.service';

@Component({
  selector: 'app-tag-button',
  templateUrl: './tag-button.component.html',
  styleUrls: ['./tag-button.component.scss']
})
export class TagButtonComponent implements OnInit {

  @Input() thisTag?:any;


  constructor() {
   }

  ngOnInit(): void {
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { ArtikelService } from '../artikel.service';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent implements OnInit {
  fathumbsup = faThumbsUp;
  constructor(public artikelService: ArtikelService) {}
  @Input() commentBlok?: any;
  ngOnInit(): void {}
}

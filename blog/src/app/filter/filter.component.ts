import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArtikelService } from '../artikel.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  tag?:any;
  searchTerm?:any;
  artikels?:any;
  gefilterdeArtikels?:any
  constructor(public route: ActivatedRoute, public artikelService: ArtikelService) {
    this.artikels = artikelService.artikels;
   }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.tag = params['tag'];
      this.searchTerm = params['srch'];
      if(this.searchTerm == undefined){
        this.gefilterdeArtikels = this.artikels.filter((x:any) => x.tags.indexOf(this.tag) !==  -1);
      }else{
        this.gefilterdeArtikels = this.artikels.filter((x:any) => x.titel.indexOf(this.searchTerm) !==  -1);
      }
    });
  }

}

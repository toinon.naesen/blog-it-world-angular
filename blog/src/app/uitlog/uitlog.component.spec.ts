import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UitlogComponent } from './uitlog.component';

describe('UitlogComponent', () => {
  let component: UitlogComponent;
  let fixture: ComponentFixture<UitlogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UitlogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UitlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

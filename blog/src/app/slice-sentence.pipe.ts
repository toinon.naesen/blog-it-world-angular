import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sliceSentence'
})
export class SliceSentencePipe implements PipeTransform {

  transform(value: string, start: number, end?: number): string {
    if (value == null){
      return '';
    } 
    else{
      return value
      .split(" ")
      .splice(start, end)
      .join(" ");
    }
  }
}


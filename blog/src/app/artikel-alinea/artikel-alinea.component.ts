import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-artikel-alinea',
  templateUrl: './artikel-alinea.component.html',
  styleUrls: ['./artikel-alinea.component.scss']
})
export class ArtikelAlineaComponent implements OnInit {

  @Input() artikelAlinea?:any;


  constructor() { }

  ngOnInit(): void {
  }

}

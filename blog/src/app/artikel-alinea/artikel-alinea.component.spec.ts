import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtikelAlineaComponent } from './artikel-alinea.component';

describe('ArtikelAlineaComponent', () => {
  let component: ArtikelAlineaComponent;
  let fixture: ComponentFixture<ArtikelAlineaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtikelAlineaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtikelAlineaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

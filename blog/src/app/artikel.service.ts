import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class ArtikelService {
  slice: any;
  constructor() {}

  nieuwArtikelID = 17;
  artikels = [
    {
      id: 1,
      titel: 'Motivatie als brandstof, goedkoper dan aardgas.',
      auteur: 'Benjamin L.',
      foto: ['assets/images/motivatie.jpg'],
      intro:
        'Onlangs ben ik gestart aan een .Net traject die ons slechts op enkele maanden tijd klaarstoomt voor een beroep als .Net developer. In het begin dacht ik wel dat deze opleiding veel tijd en energie zou vergen. Ondertussen heeft deze gedachte een kleine update gekregen.',
      afbeeldingen: [
        'assets/images/motivatie.jpg',
        '',
        'assets/images/motivatie2.jpg',
      ],
      alineas: [
        'Onlangs ben ik gestart aan een .Net traject die ons slechts op enkele maanden tijd klaarstoomt voor een beroep als .Net developer. In het begin dacht ik wel dat deze opleiding veel tijd en energie zou vergen. Ondertussen heeft deze gedachte een kleine update gekregen.',
        'Soms zijn er dagen waar alles tegenzit, niets werkt en alles wat je probeert faalt... Enorm frustrerend! Het element dat ik volledig over het hoofd gezien had is motivatie.',
        'Motivatie komt voor in verschillende vormen en omvat allerhande meningen. Voor mij is motivatie leergierigheid, een hongerig gevoel naar kennis. Elke keer wij iets nieuws leren, denk ik meteen aan het volgende: Waar kan ik dit toevoegen aan projecten die ik reeds gemaakt had? Is het toeval dat we dit leren in deze fase van onze opleiding of zit daar meer achter? En waarom verschijnt er sinds kort een glimlach op mijn gezicht als mijn code niet meteen werkt?',
        'Motivatie is een grote factor in tijden dat het wat moeilijker gaat. Het geeft je een doel om te blijven groeien en zorgt ervoor dat je niet van je pad afwijkt. De hoeveelheid leerstof die wij hebben bijgeleerd op zo’n korte periode… Ik kan alleen maar beamen hoe wonderbaarlijk dat is. Zolang je motivatie als brandstof gebruikt, kom je vroeg of laat op een plek waar je zijn wilt.',
      ],
      tussentitels: ['', ''],
      comments: [
        {
          user: 'Kristof',
          comment: 'Motivatie is zeker belangrijk!',
          leuk: 3,
          avatar: 'assets/images/avatar1.jpg',
        },
        {
          user: 'Bart',
          comment:
            'We hebben inderdaad al veel leerstof gezien. Blijven leren is belangrijk.',
          leuk: 4,
          avatar: 'assets/images/avatar2.jpg',
        },
        {
          user: 'Harald',
          comment: 'Beginnen valt niet mee, maar doorzetten wordt beloond.',
          leuk: 7,
          avatar: 'assets/images/avatar3.jpg',
        },
        {
          user: 'Jan',
          comment: 'Het is wel allemaal heel interessant.',
          leuk: 7,
          avatar: ['assets/images/avatar4.jpg'],
        },
      ],
      likes: 20,
      tags: ['C#'],
      datum: '18 Feb 2022',
    },
    {
      id: 2,
      titel: 'Ik ben trots op mijn slechtste werk',
      auteur: 'Kristof L.',
      foto: ['assets/images/kristof.jpg'],
      intro:
        'Een paar jaar geleden heb ik mijn eerste stapjes in de programmeerwereld gezet door een After Effects Script te schrijven. De bedoeling was een tool te maken die gezichtsanimatie makkelijker maakt.',
      afbeeldingen: ['assets/images/Riguru01.png', ''],
      alineas: ['Een paar jaar geleden heb ik mijn eerste stapjes in de programmeerwereld gezet door een After Effects Script te schrijven. De bedoeling was een tool te maken die gezichtsanimatie makkelijker maakt.', 
      'Wat startte als een paar honderd regels code groeide over de jaren tot ongeveer 30.000 lijnen code. Het ergste is dat dit project nog steeds niet af is! Een goede programmeur weet dat het aantal lijnen code niets zegt over de kwaliteit van software en niets is minder waar hier.', 
      'Hoe meer ik bijleer, hoe meer bewust ik word de ondermaatse kwaliteit van de code. Zoals ik al eerder heb vermeld, dit was mijn eerste programmeer-project. Doorheen de jaren ben ik beter geworden. Sinds kort ben ik een .NET developer opleiding gestart en is mijn evolutie als programmeur in een stroomversnelling geraakt. ', 
      'Dit project is een soort tijdlijn van mijn groei als programmeur. Het is een herinnering aan alle valkuilen waar ik ben ingetrapt.'
      ],
      tussentitels: [''],
      comments: [
        {
          user: 'Toinon',
          comment: 'Tof om dit te lezen!',
          avatar: 'assets/images/avatar9.jpg',
        },
        {
          user: 'Benjamin',
          comment:
            'We hebben inderdaad al veel gezien op korte tijd. Ongelofelijk!',
          avatar: 'assets/images/avatar6.jpg',
        },
      ],
      likes: 47,
      tags: ['JavaScript'],
      datum: '07 Feb 2022',
    },
    {
      id: 3,
      titel: 'Winforms C#',
      auteur: 'Toinon N.',
      foto: ['assets/images/winforms.jpg'],
      intro:
        'Na het maken van de console applicatie was de opleiding nog niet gedaan. Ik had nog maar een tipje van de ijsberg gezien of beter de fundering van het huis.',
      afbeeldingen: [
        'assets/images/winforms.jpg',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        'assets/images/winforms2.jpg',
      ],
      alineas: [
        'Na het maken van de console applicatie was de opleiding nog niet gedaan. Ik had nog maar een tipje van de ijsberg gezien of beter de fundering van het huis.',
        'Nu moest het huis verder gebouwd worden. De eerste muur begon ik te bouwen tijdens de module gevorderde C#. Hier leerde ik werken met Winforms. Winforms is een platform waar applicaties kunnen gemaakt worden voor desktops.',
        'In basic C# leerde ik werken met de code. In deze module was er niet alleen code maar het visuele aspect kwam al meer aan bod. Vanuit de toolbox in Visual Studio sleepte ik buttons, checkboxen, textboxen, comboboxen, … naar de form. Door code achter deze tools te zetten maakte ik verschillende applicaties in de les. De ene al moeilijker dan de andere.',
        'Eenmaal ik de basis onder de knie had, werd er lesgegeven over klassen. In het begin vond ik dit moeilijk, maar eenmaal ik meer en meer oefende had ik dit beter onder de knie.',
        'Na enkele weken alles gezien te hebben over Winforms was het opnieuw tijd om een project in groep uit te werken. Voor dit project hadden we een paar weken de tijd.',
        'In totaal werden er drie projecten uitgewerkt waarbij er gewerkt werd met overdracht van gegevens via XML-bestanden. Het overkoepelende thema was: ‘van cacaoboon tot praline’. Ik zat in de groep die zorgde voor de applicatie van de chocoladewinkel.',
        'Tijdens dit project werkten we met Jira om de verdeling per teamlid goed te zien. We konden hier ook de vordering van ons week bijhouden. Daarnaast werkten we niet met GitHub maar met GitLab. In het begin was het toch wat moeilijk om GitLab te gebruiken want we kregen veel errors. Naarmate we meer hiermee aan de slag gingen, kregen we veel minder errors.',
        'Voor de applicatie zorgde ik voor de kassa en het bijhouden van het assortiment. Ik werkte voor het assortiment met een datagridview. Ik had hier nog nooit mee gewerkt dus moest ik veel online opzoeken. Uiteindelijk kwam ik telkens uit op de correcte oplossing. Ook het in- en uitlezen van XML-bestanden verwerkte ik in de applicatie.',
        'Zelf was ik tevreden over de vorderingen die ik had gemaakt t.o.v. het vorig project, maar ook de lessen.',
        'Net zoals het vorig project presenteerden we onze applicatie. Ik vond het al minder spannend omdat ik wist wat er te wachten stond. Ook deze keer verliep de presentatie goed.',
        'Deze tip neem ik mee:',
        'Niemand is een wandelend woordenboek en kent alles uit het hoofd. Opzoeken op internet is dus niet erg.',
      ],
      tussentitels: [''],
      comments: [
        {
          user: 'Luis',
          comment: 'Mooi logo.',
          avatar: 'assets/images/avatar7.jpg',
        },
        {
          user: 'Benjamin',
          comment:
            'We hebben inderdaad al veel gezien op korte tijd. Ongelofelijk!',
          avatar: 'assets/images/avatar8.jpg',
        },
      ],
      likes: 26,
      tags: ['Technologie', 'C#'],
      datum: '31 Jan 2022',
    },
    {
      id: 4,
      titel: 'Console applicatie C#',
      auteur: 'Toinon N.',
      foto: ['assets/images/consoleApp2.jpg'],
      intro:
        'Na alles gezien te hebben over Console.WriteLine(), Console.ReadLine(), for-loops, variabelen, … was het tijd om aan de slag te gaan met het eerste project. Nu zul je misschien denken, dat het eerste project gemaakt wordt na een aantal maanden. Dit was helemaal niet het geval. Het eerste project maakten we al na enkele weken.  ',
      afbeeldingen: [
        'assets/images/consoleApp2.jpg',
        '',
        'assets/images/consoleApp3.jpg',
        '',
        '',
        '',
        'assets/images/consoleApp.png',
      ],
      alineas: [
        'Na alles gezien te hebben over Console.WriteLine(), Console.ReadLine(), for-loops, variabelen, … was het tijd om aan de slag te gaan met het eerste project. Nu zul je misschien denken, dat het eerste project gemaakt wordt na een aantal maanden. Dit was helemaal niet het geval. Het eerste project maakten we al na enkele weken.',
        'De dagen voor de start van het project dacht ik al na over hoe je aan een project moet beginnen. Gelukkig vertelde Hans uitgebreid hoe we in groep aan de slag moesten gaan.',
        'Stap één was nadenken over wat ons project zou gaan. Al snel waren we eraan uit. We gingen een console applicatie maken waarbij we lokale restaurants zouden promoten. Een soort take-away applicatie.',
        'Stap twee was het uitschrijven van een interview tussen de klant (het bedrijf die de app aanvroeg) en ons team.',
        'Stap drie was het maken van een schema waarbij we aangaven wie wat zou doen.',
        'De laatste stap, maar de belangrijkste, was de applicatie maken. We kregen een week de tijd om onze applicatie te coderen. Natuurlijk zorgden we ervoor dat onze delen steeds samen konden worden gevoegd. We gebruikten hiervoor GitHub. In het begin hadden we vaak problemen, maar naarmate ons project vorderde ging het steeds vlotter om GitHub te gebruiken.',
        'Na een week hard doorgewerkt te hebben aan ons project was de dag daar dat we het project, genaamd MultiResto, moesten voorstellen aan de andere cursisten van de groep. Ik vond dit spannend. Ik had al 3 jaar voor een klas gestaan, maar dit was toch anders. Na een korte intro toonden we onze applicatie. Voor mijn gevoel ging het goed en was iedereen mee in het verhaal van de take-away.',
        'Ik heb enkele tips meegenomen uit deze samenwerking:',
        '1) Het is belangrijk om eerst de noodzakelijke dingen te coderen. De extra’s kunnen nog aan de applicatie worden toegevoegd wanneer er tijd over is. Afbakenen is de boodschap!',
        '2) Een goede intro en je publiek is meteen mee met het verhaal van je applicatie.',
        '3) Het coderen in C# zelf. We hadden voordien vaak oefeningen gemaakt, maar dit was toch anders.',
        '4) Het werken aan een applicatie in het algemeen. De stappen die je moet zetten en wat er allemaal zal moeten gebeuren.',
      ],
      tussentitels: [
        '',
        '',
        'De verschillende stappen',
        '',
        '',
        '',
        'Eindresultaat',
        'Tips',
      ],
      comments: [
        {
          user: 'Steven',
          comment: 'It always seems impossible until it is done.',
          avatar: 'assets/images/avatar9.jpg',
        },
      ],
      likes: 50,
      tags: ['C#'],
      datum: '24 Jan 2022',
    },
    {
      id: 5,
      titel: 'Een nieuw begin',
      auteur: 'Toinon N.',
      foto: ['assets/images/nieuwBegin.jpg'],
      intro:
        'Donderdag 14 november 2021, het was vroeg in de ochtend. Op de planning stond mijn eerste les programmeren. Om 07.15 uur zat ik in de auto. Ik zette mijn gps aan richting Sint-Niklaas en natuurlijk een leuk muziekje. Hopelijk heb ik niet te veel file dacht ik in mezelf.',
      afbeeldingen: ['assets/images/nieuwBegin.jpg', ''],
      alineas: [
        'Donderdag 14 november 2021, het was vroeg in de ochtend. Op de planning stond mijn eerste les programmeren. Om 07.15 uur zat ik in de auto. Ik zette mijn gps aan richting Sint-Niklaas en natuurlijk een leuk muziekje. Hopelijk heb ik niet te veel file dacht ik in mezelf.',
        'Na meer dan een uur kwam ik ter plaatse aan. Gelukkig was ik er de vorige dag al eens geweest. Zo wist ik waar ik moest zijn. Er zaten al enkele medestudenten in het klaslokaal te wachten. Het was geen gewoon klaslokaal zoals je die zou kennen van op de middelbare school of de hogeschool. Laten we zeggen dat het meer leek op een congreszaal met zuilen, een podium en verschillende tafels.',
        'Eenmaal iedereen aanwezig was, maakten we kennis met elkaar. In totaal waren we met 12 studenten en 1 docent. Na een aantal kennismakingsspelletjes vlogen we erin. Hans, de docent, legde ons alles uit over het verschil tussen software en hardware, de fasen van het schrijven van software, programmatielogica, …',
        'De programmatielogica kwam mij bekend voor. Begin oktober legde ik een ingangstoets af van de VDAB. Hier kwam dit ook in voor. Op dat moment begreep ik er nog niet veel van. Na de goede uitleg van Hans kon ik me al meer vinden in deze soort logica.',
        'De tijd vloog voorbij en voor ik het wist was het al middag. Na het eten van lekkere boterhammen en een hele voormiddag binnen gezeten te hebben, maakte ik een wandeling door Sint-Niklaas samen met Jasmine, een medestudente. De buitenlucht deed me goed.',
        'Na de middagpauze was het tijd voor onze eerste les programmeren. De taal waarin we gingen programmeren was C#. Ik vond het spannend. Het ging nu echt gebeuren. Na de uitleg van Hans nam ik mijn laptop, waar ik de vorige dag Visual Studio op had geïnstalleerd. Ik opende dit programma en typte de volgende woorden: Console.WriteLine(“Hello world!”). Zo ik had mijn eerste programmeertaal geschreven. Na nog wat oefenen, was het al tijd om naar huis te gaan.',
        'Het was een leuke, maar ook spannende dag. Een dag waar het avontuur begon voor mij.',
      ],
      tussentitels: [''],
      comments: [
        {
          user: 'Hans',
          comment: 'Leuk om te zien hoe je de eerste dag beleefde.',
          avatar: 'assets/images/avatar10.jpg',
        },
        {
          user: 'Tijs',
          comment: 'We zullen zien hoe het nu allemaal gaat verlopen.',
          avatar: 'assets/images/avatar4.jpg',
        },
        {
          user: 'Gaël',
          comment: 'Veel succes!',
          avatar: 'assets/images/avatar9.jpg',
        },
      ],
      likes: 29,
      tags: ['C#'],
      datum: '17 Jan 2022',
    },
    {
      id: 6,
      titel: 'Lorem Ipsum deel 11',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=25'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=21',
        'https://picsum.photos/200/300?random=22',
        'https://picsum.photos/200/300?random=23',
        'https://picsum.photos/200/300?random=24',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 35,
      tags: ['HTML'],
      datum: '10 Jan 2022',
    },
    {
      id: 7,
      titel: 'Lorem Ipsum deel 10',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=26'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=19',
        'https://picsum.photos/200/300?random=20',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 40,
      tags: [''],
      datum: '03 Jan 2022',
    },
    {
      id: 8,
      titel: 'Lorem Ipsum deel 9',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=27'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=17',
        'https://picsum.photos/200/300?random=18',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 30,
      tags: [''],
      datum: '27 Dec 2021',
    },
    {
      id: 9,
      titel: 'Lorem Ipsum deel 8',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=28'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=15',
        'https://picsum.photos/200/300?random=16',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
        '"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
        '1914 vertaling door H. Rackham',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 45,
      tags: [''],
      datum: '20 Dec 2021',
    },
    {
      id: 10,
      titel: 'Lorem Ipsum deel 7',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=29'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=13',
        'https://picsum.photos/200/300?random=14',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
        '"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
        '1914 vertaling door H. Rackham',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 36,
      tags: [''],
      datum: '13 Dec 2021',
    },
    {
      id: 11,
      titel: 'Lorem Ipsum deel 6',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=30'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=11',
        'https://picsum.photos/200/300?random=12',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
        '"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
        '1914 vertaling door H. Rackham',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 29,
      tags: [''],
      datum: '06 Dec 2021',
    },
    {
      id: 12,
      titel: 'Lorem Ipsum deel 5',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=31'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=9',
        'https://picsum.photos/200/300?random=10',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
        '"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
        '1914 vertaling door H. Rackham',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 37,
      tags: [''],
      datum: '29 Nov 2021',
    },
    {
      id: 13,
      titel: 'Lorem Ipsum deel 4',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=32'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=7',
        'https://picsum.photos/200/300?random=8',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
        '"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
        '1914 vertaling door H. Rackham',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 46,
      tags: [''],
      datum: '22 Nov 2021',
    },
    {
      id: 14,
      titel: 'Lorem Ipsum deel 3',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=33'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=5',
        'https://picsum.photos/200/300?random=6',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
        '"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
        '1914 vertaling door H. Rackham',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 27,
      tags: [''],
      datum: '15 Nov 2021',
    },
    {
      id: 15,
      titel: 'Lorem Ipsum deel 2',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=34'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=3',
        'https://picsum.photos/200/300?random=4',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
        '"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
        '1914 vertaling door H. Rackham',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 17,
      tags: [''],
      datum: '08 Nov 2021',
    },
    {
      id: 16,
      titel: 'Lorem Ipsum deel 1',
      auteur: 'Lorem',
      foto: ['https://picsum.photos/300/300?random=35'],
      intro:
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."' +
        '"Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
      afbeeldingen: [
        'https://picsum.photos/200/300?random=1',
        'https://picsum.photos/200/300?random=2',
      ],
      alineas: [
        '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... "Er is niemand die van pijn zelf houdt, die het zoekt en die het hebben wil, eenvoudigweg omdat het pijn is..."',
        'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren 60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.',
        'In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32. Het standaard stuk van Lorum Ipsum wat sinds de 16e eeuw wordt gebruikt is hieronder, voor wie er interesse in heeft, weergegeven. Secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" door Cicero zijn ook weergegeven in hun exacte originele vorm, vergezeld van engelse versies van de 1914 vertaling door H. Rackham.',
        'Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout van een pagina, afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het uit een min of meer normale verdeling van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer leesbaar nederlands maakt. Veel desktop publishing pakketten en web pagina editors gebruiken tegenwoordig Lorem Ipsum als hun standaard model tekst, en een zoekopdracht naar "lorem ipsum" ontsluit veel websites die nog in aanbouw zijn. Verscheidene versies hebben zich ontwikkeld in de loop van de jaren, soms per ongeluk soms express (ingevoegde humor en dergelijke).',
        'Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets beschamends midden in de tekst verborgen zit. Alle Lorum Ipsum generators op Internet hebben de eigenschap voorgedefinieerde stukken te herhalen waar nodig zodat dit de eerste echte generator is op internet. Het gebruikt een woordenlijst van 200 latijnse woorden gecombineerd met een handvol zinsstructuur modellen om een Lorum Ipsum te genereren die redelijk overkomt. De gegenereerde Lorum Ipsum is daardoor altijd vrij van herhaling, ingevoegde humor of ongebruikelijke woorden etc.',
        '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
        '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"',
        '"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"',
      ],
      tussentitels: [
        '',
        'Wat is Lorem Ipsum?',
        'Waar komt het vandaan?',
        'Waarom gebruiken we het?',
        'Waar kan ik het vinden?',
        'De standaard Lorem Ipsum passage, in gebruik sinds de 16e eeuw',
        'Sectie 1.10.32 van "de Finibus Bonorum et Malorum", geschreven door Cicero in 45 v.Chr.',
        '1914 vertaling door H. Rackham',
      ],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 9,
      tags: [''],
      datum: '07 Nov 2021',
    },
  ];
  gesorteerdeArtikels?: any;
  meestGeliketeArtikel?: any;
  meestGeliketeArtikels: any = [];
  vulMeestGeliketeArtikels() {
    this.gesorteerdeArtikels = [...this.artikels];
    this.gesorteerdeArtikels.sort(this.compare);
    this.meestGeliketeArtikels.length = 0;
    this.meestGeliketeArtikel = this.gesorteerdeArtikels[0];
    for (let index = 1; index < 5; index++) {
      this.meestGeliketeArtikels.push(this.gesorteerdeArtikels[index]);
    }
  }

  ArtikelToevoegen(dezetitel: string, bericht: string, tag: string[]) {
    if(this.ingelogdeGebruiker == "gast"){
        return false;
    }
    this.artikels.unshift({
      id: this.nieuwArtikelID,
      titel: dezetitel,
      auteur: this.ingelogdeGebruiker,
      foto: ['https://picsum.photos/200/300?random=1'],
      intro: bericht,
      afbeeldingen: ['', ''],
      alineas: [bericht],
      tussentitels: [''],
      comments: [
        {
          user: 'Kristof',
          comment: 'Hey!',
          avatar: 'assets/images/avatar8.jpg',
        },
        { user: 'Bart', comment: 'Lol!', avatar: 'assets/images/avatar9.jpg' },
      ],
      likes: 100,
      tags: tag,
      datum: '18 Feb 2022',
    });
    this.nieuwArtikelID++;
    return true;

  }

  compare(a: any, b: any) {
    if (a.likes > b.likes) {
      return -1;
    }
    if (a.likes < b.likes) {
      return 1;
    }
    return 0;
  }

  tags = ['Technologie', 'Programmeren', 'C#', 'JavaScript', 'HTML', 'Angular'];
  logins = [
    { gebruikersnaam: 'Kristof', wachtwoord: '123' },
    { gebruikersnaam: 'Toinon', wachtwoord: '456' },
    { gebruikersnaam: 'Benjamin', wachtwoord: '789' },
  ];
  ingelogdeGebruiker = 'gast';
  VerificeerLogin(gebruikersnaam: string, wachtwoord: string) {
    let dezeGebruiker = this.logins.find(
      (x) => x.gebruikersnaam == gebruikersnaam && x.wachtwoord == wachtwoord
    );
    if (dezeGebruiker == undefined) {
      alert(
        'Er is geen gebruiker gevonden met deze gebruikersnaam en dit wachtwoord.'
      );
      return false;
    } else {
      this.ingelogdeGebruiker = dezeGebruiker.gebruikersnaam;
      return true;
    }
  }
  VerwijderArtikel(id: number) {
    for (let index = 0; index < this.artikels.length; index++) {
      if (this.artikels[index].id == id) {
        this.artikels.splice(index, 1);
      }
    }
  }
  LogOut() {
    if (this.ingelogdeGebruiker !== 'gast') {
      this.ingelogdeGebruiker = 'gast';
      return true;
    }
    return false;
  }
}

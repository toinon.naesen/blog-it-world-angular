import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeestGeliketeArtikelComponent } from './meest-gelikete-artikel.component';

describe('MeestGeliketeArtikelComponent', () => {
  let component: MeestGeliketeArtikelComponent;
  let fixture: ComponentFixture<MeestGeliketeArtikelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeestGeliketeArtikelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeestGeliketeArtikelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ArtikelService } from '../artikel.service';

@Component({
  selector: 'app-meest-gelikete-artikel',
  templateUrl: './meest-gelikete-artikel.component.html',
  styleUrls: ['./meest-gelikete-artikel.component.scss']
})
export class MeestGeliketeArtikelComponent implements OnInit {

  meestGelikteArtikel?:any;
  constructor(public artikelService:ArtikelService) {
    this.meestGelikteArtikel = artikelService.meestGeliketeArtikel
   }
   
  ngOnInit(): void {
  }

}

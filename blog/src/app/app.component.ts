import { Component, OnInit, Renderer2, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArtikelService } from './artikel.service';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { DecimalPipe, DOCUMENT } from '@angular/common';

gsap.registerPlugin(ScrollTrigger);
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'blog';
  artikels;
  public reachedTheEnd: boolean = false;
  public theme: Theme = 'light-theme';

  constructor(
    private route: ActivatedRoute,
    public artikelService: ArtikelService,
    private decimalPipe: DecimalPipe,
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2
  ) {
    this.artikels = artikelService.artikels;
  }

  ngOnInit() {
    this.initializeTheme();
    gsap.to('progress', {
      value: 100,
      scrollTrigger: {
        trigger: '.main',
        scrub: 0.3,
        start: 'top top',
        end: 'bottom bottom',
        onUpdate: (options) => {
          if (options instanceof ScrollTrigger) {
            const value = Number(
              this.decimalPipe.transform(options.progress, '1.2-2')
            );
            this.reachedTheEnd = value > 0.5;
          }
        },
      },
    });
  }

  public switchTheme() {
    this.document.body.classList.replace(
      this.theme,
      this.theme === 'light-theme'
        ? (this.theme = 'dark-theme')
        : (this.theme = 'light-theme')
    );
  }
  initializeTheme = (): void =>
    this.renderer.addClass(this.document.body, this.theme);

  onClick(elem: HTMLElement) {
    elem.scrollIntoView({ behavior: 'smooth' });
  }
}

export type Theme = 'light-theme' | 'dark-theme';

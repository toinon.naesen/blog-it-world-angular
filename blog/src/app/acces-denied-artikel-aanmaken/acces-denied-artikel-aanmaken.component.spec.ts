import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccesDeniedArtikelAanmakenComponent } from './acces-denied-artikel-aanmaken.component';

describe('AccesDeniedArtikelAanmakenComponent', () => {
  let component: AccesDeniedArtikelAanmakenComponent;
  let fixture: ComponentFixture<AccesDeniedArtikelAanmakenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccesDeniedArtikelAanmakenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccesDeniedArtikelAanmakenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

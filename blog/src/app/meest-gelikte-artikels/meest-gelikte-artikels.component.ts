import { Component, OnInit } from '@angular/core';
import { ArtikelService } from '../artikel.service';

@Component({
  selector: 'app-meest-gelikte-artikels',
  templateUrl: './meest-gelikte-artikels.component.html',
  styleUrls: ['./meest-gelikte-artikels.component.scss']
})
export class MeestGelikteArtikelsComponent implements OnInit {

  meestGeliketeArtikels?:any;
  meestGeliketArtikel?:any
  constructor(public artikelService:ArtikelService) 
  {
    artikelService.vulMeestGeliketeArtikels();
   this.meestGeliketeArtikels = artikelService.meestGeliketeArtikels
   }

  ngOnInit(): void {
  }

}

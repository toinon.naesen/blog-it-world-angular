import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeestGelikteArtikelsComponent } from './meest-gelikte-artikels.component';

describe('MeestGelikteArtikelsComponent', () => {
  let component: MeestGelikteArtikelsComponent;
  let fixture: ComponentFixture<MeestGelikteArtikelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeestGelikteArtikelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeestGelikteArtikelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

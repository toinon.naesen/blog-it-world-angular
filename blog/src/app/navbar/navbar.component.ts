import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { faMoon } from '@fortawesome/free-solid-svg-icons';
import { faSun } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { ArtikelService } from '../artikel.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  collapsed = true;
  faBars = faBars;
  isDarkTheme: boolean = false;
  theme: Theme = 'light-theme';
  famoon = faMoon;
  fasun = faSun;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    public artikelService: ArtikelService
  ) {}

  ngOnInit(): void {
    this.initializeTheme();
  }

  switchTheme() {
    this.document.body.classList.replace(
      this.theme,
      this.theme === 'light-theme'
        ? (this.theme = 'dark-theme')
        : (this.theme = 'light-theme')
    );
    document.getElementById('sun')?.classList.toggle('displayNone');
    document.getElementById('moon')?.classList.toggle('displayNone');
  }

  LogOut(){
    let wasLoggedIn = this.artikelService.LogOut();

    if(wasLoggedIn){
      document.getElementById("uitlogMessage")?.classList.remove('noDisplay');
      document.getElementById("uitlogMessage")?.classList.add('overLayShow');
      setTimeout(function () {
        document.getElementById("uitlogMessage")?.classList.add('noDisplay');
        document.getElementById("uitlogMessage")?.classList.remove('overLayShow');
    }, 2000);
    }

  }


  initializeTheme = (): void =>
    this.renderer.addClass(this.document.body, this.theme);
}

export type Theme = 'light-theme' | 'dark-theme';

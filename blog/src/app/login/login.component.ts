import { Component, OnInit } from '@angular/core';
import { ArtikelService } from '../artikel.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  logins?: any;
  constructor(public artikelService: ArtikelService) {}

  VerificeerLogin() {
    let logidSucceeded = this.artikelService.VerificeerLogin(((<HTMLInputElement>document.getElementById("gebruikersnaam")).value), ((<HTMLInputElement>document.getElementById("wachtwoord")).value));
    if(logidSucceeded){
      document.getElementById('overlayMessage')?.classList.remove('noDisplay');
      document.getElementById('overlayMessage')?.classList.add('overLayShow');
    }
  }

  SluitVenster() {
    document.getElementById('overlayMessage')?.classList.remove('overLayShow');
    document.getElementById('overlayMessage')?.classList.add('noDisplay');
  }

  ngOnInit(): void {}
}

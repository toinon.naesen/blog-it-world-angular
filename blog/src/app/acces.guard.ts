import { Injectable } from '@angular/core';
import {CanActivate} from '@angular/router';
import { Observable } from 'rxjs';
import { ArtikelService } from './artikel.service';

@Injectable({
  providedIn: 'root'
})
export class AccesGuard implements CanActivate {
constructor(public artikelService: ArtikelService){

}
  canActivate(){
    if(this.artikelService.ingelogdeGebruiker == "gast"){
        document.getElementById("accesDeniedArtikelAanmaken")?.classList.remove('noDisplay');
        document.getElementById("accesDeniedArtikelAanmaken")?.classList.add('overLayShowDenied');
        setTimeout(function () {
          document.getElementById("accesDeniedArtikelAanmaken")?.classList.add('noDisplay');
          document.getElementById("accesDeniedArtikelAanmaken")?.classList.remove('overLayShowDenied');
      }, 6000);
      
    }
    return (this.artikelService.ingelogdeGebruiker !== "gast");
  }
}

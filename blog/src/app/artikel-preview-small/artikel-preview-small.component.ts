import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-artikel-preview-small',
  templateUrl: './artikel-preview-small.component.html',
  styleUrls: ['./artikel-preview-small.component.scss']
})
export class ArtikelPreviewSmallComponent implements OnInit {

  @Input() artikel?:any;

  constructor() { }

  ngOnInit(): void {
  }

}

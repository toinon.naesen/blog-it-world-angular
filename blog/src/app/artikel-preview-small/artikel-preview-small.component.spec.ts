import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtikelPreviewSmallComponent } from './artikel-preview-small.component';

describe('ArtikelPreviewSmallComponent', () => {
  let component: ArtikelPreviewSmallComponent;
  let fixture: ComponentFixture<ArtikelPreviewSmallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtikelPreviewSmallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtikelPreviewSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

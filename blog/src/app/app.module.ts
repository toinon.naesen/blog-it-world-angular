import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ArtikelComponent } from './artikel/artikel.component';
import { ArtikelAanmakenComponent } from './artikel-aanmaken/artikel-aanmaken.component';
import { ArtikelPreviewComponent } from './artikel-preview/artikel-preview.component';
import { CommentComponent } from './comment/comment.component';
import { Error404Component } from './error404/error404.component';
import { ArtikelAlineaComponent } from './artikel-alinea/artikel-alinea.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LastUpdatedPipe } from './last-updated.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { NgxScrollTopModule } from 'ngx-scrolltop';
import { DecimalPipe } from '@angular/common';
import { MeestGelikteArtikelsComponent } from './meest-gelikte-artikels/meest-gelikte-artikels.component';
import { ArtikelPreviewSmallComponent } from './artikel-preview-small/artikel-preview-small.component';
import { MeestGeliketeArtikelComponent } from './meest-gelikete-artikel/meest-gelikete-artikel.component';
import { TagSectionComponent } from './tag-section/tag-section.component';
import { TagButtonComponent } from './tag-button/tag-button.component';
import { FilterComponent } from './filter/filter.component';
import { SliceSentencePipe } from './slice-sentence.pipe';
import { LoginComponent } from './login/login.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LikeComponent } from './like/like.component';
import { ZoekbalkComponent } from './zoekbalk/zoekbalk.component';
import { UitlogComponent } from './uitlog/uitlog.component';
import { AccesDeniedArtikelAanmakenComponent } from './acces-denied-artikel-aanmaken/acces-denied-artikel-aanmaken.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ArtikelComponent,
    ArtikelPreviewComponent,
    CommentComponent,
    Error404Component,
    ArtikelAlineaComponent,
    LastUpdatedPipe,
    NavbarComponent,
    MeestGelikteArtikelsComponent,
    ArtikelPreviewSmallComponent,
    MeestGeliketeArtikelComponent,
    TagSectionComponent,
    TagButtonComponent,
    FilterComponent,
    SliceSentencePipe,
    LoginComponent,
    ArtikelAanmakenComponent,
    LikeComponent,
    ZoekbalkComponent,
    UitlogComponent,
    AccesDeniedArtikelAanmakenComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule,
    NgxScrollTopModule,
    FontAwesomeModule,
  ],
  providers: [DecimalPipe],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtikelPreviewComponent } from './artikel-preview.component';

describe('ArtikelPreviewComponent', () => {
  let component: ArtikelPreviewComponent;
  let fixture: ComponentFixture<ArtikelPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtikelPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtikelPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

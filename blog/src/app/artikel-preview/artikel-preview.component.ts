import { Component, OnInit, Input } from '@angular/core';
import { ArtikelService } from '../artikel.service';
import { faComment } from '@fortawesome/free-solid-svg-icons';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-artikel-preview',
  templateUrl: './artikel-preview.component.html',
  styleUrls: ['./artikel-preview.component.scss'],
})
export class ArtikelPreviewComponent implements OnInit {
  @Input() artikel?: any;
  faComment = faComment;
  fathumbsUp = faThumbsUp;
  constructor(public artikelService: ArtikelService) {}

  ngOnInit(): void {}
}

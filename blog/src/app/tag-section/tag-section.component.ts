import { Component, OnInit } from '@angular/core';
import { ArtikelService } from '../artikel.service';

@Component({
  selector: 'app-tag-section',
  templateUrl: './tag-section.component.html',
  styleUrls: ['./tag-section.component.scss']
})
export class TagSectionComponent implements OnInit {
  tags?:any;
  constructor(public artikelService: ArtikelService) { 
    this.tags = artikelService.tags;

  }

  ngOnInit(): void {
  }

}

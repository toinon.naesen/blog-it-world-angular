import { Component, OnInit } from '@angular/core';
import { ArtikelService } from '../artikel.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  artikels;
  aantal = 5;
  tag?:any;
  constructor(public artikelService:ArtikelService, public route:ActivatedRoute){
    this.artikels = [];
    for (let index = 0; index < 5; index++) {
      this.artikels.push(artikelService.artikels[index]);
    }

  }

  aantalArtikels:number = this.artikelService.artikels.length;
  aantalArtikelsZichtbaar:number = 5;


  UpdateArtikels(){

    for (let index = this.aantalArtikelsZichtbaar; index < this.aantalArtikelsZichtbaar+5; index++) {
      if(index>=this.aantalArtikels){
        
        break;
      }
      this.artikels.push(this.artikelService.artikels[index]);  
    }
    this.aantalArtikelsZichtbaar+=5;
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.tag = params['tag'];
      let lolz = this.artikels.filter((x) => x.tags.indexOf(this.tag) !==  -1);
    });
  }

}
